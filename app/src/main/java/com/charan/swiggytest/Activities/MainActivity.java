package com.charan.swiggytest.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.charan.swiggytest.AppController;
import com.charan.swiggytest.Constants.ApiEndPoints;
import com.charan.swiggytest.Constants.Config;
import com.charan.swiggytest.Models.ExcludeItem;
import com.charan.swiggytest.Models.VariantGroup;
import com.charan.swiggytest.Models.Variation;
import com.charan.swiggytest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ArrayList<VariantGroup> variantGroups = new ArrayList<>();
    private ArrayList<ArrayList<ExcludeItem>> excludeGroup = new ArrayList<>();

    private RadioGroup variantRadioGroup;
    private Button continueButton;
    private Button changeSelectionButton;

    private int selectedGroupIndex = 0;
    private int selectedGroupId = -1;
    private int selectedVariationId = -1;

    private int prevGroupId = -1;
    private int prevVariationId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        variantRadioGroup = (RadioGroup)findViewById(R.id.variant_radio_group);
        continueButton = (Button)findViewById(R.id.continue_btn);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGroupIndex++;
                onSelectOption();
            }
        });

        changeSelectionButton = (Button)findViewById(R.id.change_btn);
        changeSelectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGroupIndex = 0;
                selectedVariationId = -1;
                prevGroupId = -1;
                prevVariationId = -1;
                onSelectOption();
            }
        });

        getVariants();


    }

    private void onSelectOption(){

        if(selectedGroupIndex >= variantGroups.size()){
            continueButton.setVisibility(View.GONE);
            return;
        }else if(selectedGroupIndex == 0){
            changeSelectionButton.setVisibility(View.INVISIBLE);
            continueButton.setVisibility(View.VISIBLE);
        }else {
            changeSelectionButton.setVisibility(View.VISIBLE);
        }

        variantRadioGroup.removeAllViews();

        prevGroupId = selectedGroupId;
        prevVariationId = selectedVariationId;

        ArrayList<Variation> variations = variantGroups.get(selectedGroupIndex).getVariations();
        selectedGroupId = variantGroups.get(selectedGroupIndex).getGroupId();

        for(int i=0; i<variations.size(); i++){
            final Variation variation = variations.get(i);
            RadioButton radioButton = new RadioButton(this);
            radioButton.setId(variation.getId());
            radioButton.setText(variation.getName() + "(Price: Rs" + variation.getPrice() + ")" + " (In Stock: " + variation.getInStock() + ")");
            if(variation.getDefaultVal() == 1){
                radioButton.setChecked(true);
                selectedVariationId = variation.getId();
            }else {
                radioButton.setChecked(false);
            }



            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b){
                        selectedVariationId = variation.getId();
                    }
                }
            });

            boolean check = true;
            for(int j=0; j<excludeGroup.size(); j++){
                ArrayList<ExcludeItem> excludeItems = excludeGroup.get(j);

                int count = 0;
                for(int k=0; k<excludeItems.size(); k++){

                    ExcludeItem excludeItem = excludeItems.get(k);

                    if(excludeItem.getGroupId() == selectedGroupId && excludeItem.getVariationId() == variation.getId()){
                        count++;
                    }

                    if(excludeItem.getGroupId() == prevGroupId && excludeItem.getVariationId() == prevVariationId){
                        count++;
                    }

                    if(count > 1){
                        check = false;
                        break;
                    }

                }


            }

            if(check){
                variantRadioGroup.addView(radioButton);
            }

        }

    }



    private void getVariants(){
        variantGroups.clear();
        excludeGroup.clear();
        selectedVariationId = -1;
        selectedGroupIndex = 0;
        prevVariationId = -1;
        prevGroupId = -1;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, ApiEndPoints.GET_VARIANTS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(Config.isDebug){
                    Log.d(TAG, response.toString());
                }
                try{

                    JSONObject variantsData = response.getJSONObject("variants");

                    JSONArray variantGroupsData = variantsData.getJSONArray("variant_groups");
                    JSONArray excludeListData = variantsData.getJSONArray("exclude_list");

                    for(int i=0; i<variantGroupsData.length(); i++){
                        JSONObject variantGroupData = variantGroupsData.getJSONObject(i);
                        JSONArray variationsData = variantGroupData.getJSONArray("variations");
                        ArrayList<Variation> variations = new ArrayList<>();
                        for(int j=0; j<variationsData.length(); j++){
                            JSONObject variationObj = variationsData.getJSONObject(j);
                            variations.add(new Variation(variationObj.getString("name"), variationObj.getInt("price"), variationObj.getInt("default"), variationObj.getInt("id"), variationObj.getInt("inStock")));
                        }

                        variantGroups.add(new VariantGroup(variantGroupData.getInt("group_id"), variantGroupData.getString("name"), variations));
                    }

                    for(int i=0; i<excludeListData.length(); i++){
                        JSONArray excludeArray = excludeListData.getJSONArray(i);
                        ArrayList<ExcludeItem> excludeItems = new ArrayList<>();
                        for(int j=0; j<excludeArray.length(); j++){
                            excludeItems.add(new ExcludeItem(excludeArray.getJSONObject(j).getInt("group_id"), excludeArray.getJSONObject(j).getInt("variation_id")));
                        }
                        excludeGroup.add(excludeItems);
                    }


                    if(variantGroups.size() > 0){
                        onSelectOption();
                    }


                }catch (JSONException e){
                    if(Config.isDebug){
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(Config.isDebug){
                    Log.e(TAG, error.toString());
                }
                Toast.makeText(MainActivity.this, "Sorry! Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }


}
