package com.charan.swiggytest;

import android.app.Application;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by charan on 27/08/17.
 */

public class AppController extends Application {

    private static AppController mInstance;

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    public static final int initialTimeoutMs = 6000;


    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        request.setRetryPolicy(new DefaultRetryPolicy(initialTimeoutMs, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(request);
    }


}
