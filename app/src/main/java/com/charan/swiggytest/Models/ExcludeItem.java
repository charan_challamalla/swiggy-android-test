package com.charan.swiggytest.Models;

/**
 * Created by charan on 27/08/17.
 */

public class ExcludeItem {

    private int groupId;
    private int variationId;

    public ExcludeItem(int groupId, int variationId){
        this.groupId = groupId;
        this.variationId = variationId;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getVariationId() {
        return variationId;
    }
}
