package com.charan.swiggytest.Models;

import java.util.ArrayList;

/**
 * Created by charan on 27/08/17.
 */

public class VariantGroup {

    private int groupId;
    private String name;
    private ArrayList<Variation> variations;


    public VariantGroup(int groupId, String name, ArrayList<Variation> variations){
        this.groupId = groupId;
        this.name = name;
        this.variations = variations;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Variation> getVariations() {
        return variations;
    }

    public int getGroupId() {
        return groupId;
    }
}
