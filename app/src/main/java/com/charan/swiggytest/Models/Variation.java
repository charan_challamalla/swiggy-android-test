package com.charan.swiggytest.Models;

/**
 * Created by charan on 27/08/17.
 */

public class Variation {

    private String name;
    private int price;
    private int defaultVal;
    private int id;
    private int inStock;


    public Variation(String name, int price, int defaultVal, int id, int inStock){
        this.name = name;
        this.price = price;
        this.defaultVal = defaultVal;
        this.id = id;
        this.inStock = inStock;
    }

    public int getDefaultVal() {
        return defaultVal;
    }

    public int getId() {
        return id;
    }

    public int getInStock() {
        return inStock;
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }


}
